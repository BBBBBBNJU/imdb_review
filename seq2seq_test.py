'''Following code mostly adapted from
https://github.com/a7b23/text-classification-in-pytorch-using-lstm/blob/master/testing.py
'''
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import pickle
import numpy as np
from sklearn import metrics

class Model(torch.nn.Module) :
    def __init__(self,embedding_dim,hidden_dim) :
        super(Model,self).__init__()
        self.hidden_dim = hidden_dim
        self.embeddings = nn.Embedding(vocabLimit+1, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim,hidden_dim)
        self.linearOut = nn.Linear(hidden_dim,2)
    def forward(self,inputs,hidden) :
        x = self.embeddings(inputs).view(len(inputs),1,-1)
        lstm_out,lstm_h = self.lstm(x,hidden)
        x = lstm_out[-1]
        x = self.linearOut(x)
        x = F.log_softmax(x)
        return x,lstm_h
    def init_hidden(self) :
        return (Variable(torch.zeros(1, 1, self.hidden_dim)),Variable(torch.zeros(1, 1, self.hidden_dim)))

vocabLimit = 50000
max_sequence_len = 500
model = Model(50,100)

with open('dict.pkl','rb') as f :
    word_dict = pickle.load(f)
model.load_state_dict(torch.load('./seq2seq_model/model3.pth'))
# f1 = open('seq2seq_results.csv','w')
# f1.write('"id","sentiment"'+'\n')

test_pos = pickle.load(open('test_pos_stem', 'rb'))
test_neg = pickle.load(open('test_neg_stem', 'rb'))
test_set = test_pos + test_neg
test_label = np.ones(len(test_pos)).tolist() + np.zeros(len(test_neg)).tolist()
test_prob = []
for idx, data in enumerate(test_set) :
    input_data = []
    for word in data.split(' ') :
        if not word in word_dict :
            input_data.append(vocabLimit)
        else :
            input_data.append(word_dict[word])
    if len(input_data) > max_sequence_len :
            input_data = input_data[0:max_sequence_len]
    input_data = Variable(torch.LongTensor(input_data))
    hidden = model.init_hidden()
    y_pred,_ = model(input_data,hidden)
    test_prob.append(F.softmax(y_pred).data.numpy()[0][1])
#     pred1 = y_pred.data.max(1)[1].numpy()
#     f1.write(str(idx) + ',' + str(pred1) + '\n')
# f1.close()
fpr, tpr, thresholds = metrics.roc_curve(test_label, test_prob, pos_label=1)
print(metrics.auc(fpr, tpr))
