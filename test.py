from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer


wordnet_lemmatizer = WordNetLemmatizer()
porter_stemmer = PorterStemmer()
a = 'studying'
b = 'studies'

a_1 = wordnet_lemmatizer.lemmatize(a)
b_1 = wordnet_lemmatizer.lemmatize(b)

a_1 = porter_stemmer.stem(a)
b_1 = porter_stemmer.stem(b)

print(a_1)
print(b_1)
