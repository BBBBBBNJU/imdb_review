# imdb_review
This repository is for imdb sentiment classification

### Basic requirements ###
Python 3.5+, sklearn, NLTK, gensim, pytorch, numpy, stop_words, datetime

### Usage ###
 1. Download IMDB dataset from: http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz
 2. Download Google Word2Vec embeddings from: https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing
 3. Unzip both files / folders.
 4. Run pre_process.py for preprocessing
 5. Run ngram_based_learning.py for ngram based classification, and you should expect a 0.921 AUC result with default parameters setting.
 6. Run seq2seq_learning.py for deep recurrent neural network (RNN) training. It is a basic LSTM RNN network, please refer to [this page](http://colah.github.io/posts/2015-08-Understanding-LSTMs/) for the introduction of LSTM. The whole program will take about 4 hours with a 16 CPU server.
 7. Run seq2seq_test.py for RNN model test, and you should get a 0.933 AUC with default parameters setting.
