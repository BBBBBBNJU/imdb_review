'''Following code mostly adapted from
https://github.com/a7b23/text-classification-in-pytorch-using-lstm/blob/master/main.py
'''
import time
import datetime
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim
import torch.nn.functional as F
import pickle
import numpy as np
import random

class wordIndex(object) :
    def __init__(self) :
        self.count = 0
        self.word_to_idx = {}
        self.word_count = {}
    def add_word(self,word) :
        if not word in self.word_to_idx :
            self.word_to_idx[word] = self.count
            self.word_count[word] = 1
            self.count +=1
        else :
            self.word_count[word]+=1
    def add_text(self,text) :
        for word in text.split(' ') :
            self.add_word(word)


def limitDict(limit,classObj) :
    dict1 = sorted(classObj.word_count.items(),key = lambda t : t[1], reverse = True)
    count = 0
    for x,y in dict1 :
        if count >= limit-1 :
            classObj.word_to_idx[x] = limit
        else :
            classObj.word_to_idx[x] = count
        count+=1

vocabLimit = 50000
max_sequence_len = 500
obj1 = wordIndex()

train_pos = pickle.load(open('train_pos_stem', 'rb'))
train_neg = pickle.load(open('train_neg_stem', 'rb'))
train_set = train_pos + train_neg
train_label = np.ones(len(train_pos)).tolist() + np.zeros(len(train_neg)).tolist()

for eachone in train_set:
    obj1.add_text(eachone)

limitDict(vocabLimit,obj1)

class Model(torch.nn.Module) :
    def __init__(self,embedding_dim,hidden_dim) :
        super(Model,self).__init__()
        self.hidden_dim = hidden_dim
        self.embeddings = nn.Embedding(vocabLimit+1, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim,hidden_dim)
        self.linearOut = nn.Linear(hidden_dim,2)
    def forward(self,inputs,hidden) :
        x = self.embeddings(inputs).view(len(inputs),1,-1)
        lstm_out,lstm_h = self.lstm(x,hidden)
        x = lstm_out[-1]
        x = self.linearOut(x)
        x = F.log_softmax(x)
        return x,lstm_h
    def init_hidden(self) :
        return (Variable(torch.zeros(1, 1, self.hidden_dim)),Variable(torch.zeros(1, 1, self.hidden_dim)))


model = Model(50,100)
loss_function = nn.NLLLoss()
optimizer = optim.Adam(model.parameters(), lr=1e-3)
epochs = 4

torch.save(model.state_dict(), './seq2seq_model/model' + str(0)+'.pth')
print('starting training')
start_time = time.time()
for i in range(epochs):
    train_combined = list(zip(train_set, train_label))
    random.shuffle(train_combined)
    train_set[:], train_label[:] = zip(*train_combined)
    avg_loss = 0.0
    for idx, data in enumerate(train_set):
        target = train_label[idx]
        input_data = [obj1.word_to_idx[word] for word in data.split(' ')]
        if len(input_data) > max_sequence_len :
            input_data = input_data[0:max_sequence_len]

        input_data = Variable(torch.LongTensor(input_data))
        target_data = Variable(torch.LongTensor([target]))
        hidden = model.init_hidden()
        y_pred, _ = model(input_data, hidden)
        model.zero_grad()
        loss = loss_function(y_pred,target_data)
        avg_loss += loss.item()

        if idx % 500 == 0 or idx == 1:
            time_step = str(datetime.timedelta(seconds=time.time() - start_time))
            print('epoch: ' + str(i) + ' iterations: '+str(idx)+' loss: '+str(loss.item()) + ', time: '+ time_step)
        loss.backward()
        optimizer.step()
    torch.save(model.state_dict(), './seq2seq_model/model' + str(i+1)+'.pth')
    print('the average loss after completion of %d epochs is %g'%((i+1),(avg_loss/len(train_set))))

with open('dict.pkl','wb') as f :
    pickle.dump(obj1.word_to_idx,f)

