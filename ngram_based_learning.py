import numpy as np
import pickle
from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

train_pos = pickle.load(open('train_pos_stem', 'rb'))
train_neg = pickle.load(open('train_neg_stem', 'rb'))
test_pos = pickle.load(open('test_pos_stem', 'rb'))
test_neg = pickle.load(open('test_neg_stem', 'rb'))

train_label = np.ones(len(train_pos)).tolist() + np.zeros(len(train_neg)).tolist()
test_label = np.ones(len(test_pos)).tolist() + np.zeros(len(test_neg)).tolist()
train_set = train_pos + train_neg
test_set = test_pos + test_neg

# tfidf bigram vectorizer, it can be replaced by ordinary vectorizer CountVectorizer or Hashing vectorizer HashingVectorizer
# please refer to http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html
# for the definition of min_df and max_df
#
vectorizer = TfidfVectorizer(ngram_range=(2,2), decode_error='ignore', min_df=10, max_df=500)
# we use logistic regression as our classifier, we can also use svm
clf = LogisticRegression(class_weight='balanced')

train_vec = vectorizer.fit_transform(train_set).toarray()
test_vec = vectorizer.transform(test_set).toarray()

clf.fit(train_vec, train_label)
temp_result = clf.predict_proba(test_vec)
test_prob = []
for eachresult in temp_result:
    test_prob.append(eachresult[1])

fpr, tpr, thresholds = metrics.roc_curve(test_label, test_prob, pos_label=1)
print(metrics.auc(fpr, tpr))
