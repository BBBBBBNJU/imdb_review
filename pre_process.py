import pickle
import os
import nltk
import sklearn
import sys
import re
from gensim.models import KeyedVectors
from stop_words import get_stop_words
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer

en_stop_words = get_stop_words('en')
model = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)
wordnet_lemmatizer = WordNetLemmatizer()
porter_stemmer = PorterStemmer()

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, ' ', raw_html)
    return cleantext

def review_process(review):
    lower_review = review.lower()
    no_html_review = cleanhtml(lower_review)
    all_single_tokens = word_tokenize(no_html_review)
    clean_text = []
    stem_text = []
    for each_token in all_single_tokens:
        lemma_token = wordnet_lemmatizer.lemmatize(each_token)
        if lemma_token not in en_stop_words and lemma_token.isalpha():
            try:
                model[lemma_token]
                clean_text.append(lemma_token)
            except:
                pass
            stem_text.append(porter_stemmer.stem(lemma_token))
    return ' '.join(clean_text), ' '.join(stem_text)

def folder_process(folder_dir, save_dump_name):
    file_names = os.listdir(folder_dir)
    clean_text_list = []
    stem_text_list = []
    for each_file_name in file_names:
        file_dir = folder_dir + each_file_name
        reader = open(file_dir, 'r')
        raw_review_text = ''
        for eachline in reader:
            raw_review_text += eachline.strip() + ' '
        clean_text, stem_text = review_process(raw_review_text)
        clean_text_list.append(clean_text)
        stem_text_list.append(stem_text)
    pickle.dump(clean_text_list, open(save_dump_name + '_clean', 'wb'))
    # pickle.dump(stem_text_list, open(save_dump_name + '_stem', 'wb'))

train_pos_dir = './aclImdb/train/pos/'
train_neg_dir = './aclImdb/train/neg/'
test_pos_dir = './aclImdb/test/pos/'
test_neg_dir = './aclImdb/test/neg/'
folder_process(train_pos_dir, 'train_pos')
folder_process(train_neg_dir, 'train_neg')
folder_process(test_pos_dir, 'test_pos')
folder_process(test_neg_dir, 'test_neg')
